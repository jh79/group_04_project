# Netcap Latency Analyser

## Setting up Development Environment in Visual Studio Code

### Prerequisites
1. Ensure you have [VSCode](https://code.visualstudio.com/) installed on your machine.
2. Install the [Remote - SSH extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh) in VSCode.

### Step-by-Step Instructions
1. Open the terminal or command prompt on your local machine.

2. Navigate to the directory where your Vagrantfile is located.

3. Start your Vagrant virtual machine by running the following command:

```bash
vagrant up && vagrant ssh-config > ssh-config
```


5. Open VSCode and ensure you have the Remote - SSH extension installed.

6. Click on the Extensions icon in the sidebar (or press `Ctrl+Shift+X`), search for "Remote - SSH," and click "Install" to install the extension if it's not already installed.

7. Now, open the Command Palette in VSCode by pressing `Ctrl+Shift+P`1 (Windows/Linux) or `Cmd+Shift+P` (Mac).

8. In the Command Palette, search for "Remote-SSH: Open Configuration File..." and select it, then click "Settings" to open the VSCode SSH configuration settings

9. Locate and paste in the path for the `ssh-config` file that you generated earlier. This file contains the SSH configuration for our VMs.

10. Once you select the file, click on the Remote SSH icon to the left, if you still don't see the Vagrant ssh sessions, click on the refresh button.

11. Once you see the ssh connections, select the one you want to connect to, then simply click on that ssh connection. Note that it is reccomended to open to a new window as you would need the main window for git operations

12. Once in the ssh session, you will be able to select a folder you would want to operate in, select `/home/vagrant/vagrant`.

13. If you want to work with git operations, make sure you set your correct credentials as displayed on git, otherwise you would be noted as a new account. Configure your user settings by running the following commands:
```bash
git config --global user.name "NAME HERE"
git config --global user.email "EMAIL HERE"
```

14. You can now start working with your Vagrant development environment within VSCode.


That's it! You have successfully set up your SSH development environment with the Vagrant SSH configuration in Visual Studio Code. Happy coding!

---

## Contributors
### Adrian Cheng (acheng27)
acheng27@illinois.edu

### Batuhan Usluel (busluel2)
busluel2@illinois.edu

### Zijing Wei (zijingw4)
zijingw4@illinois.edu 

### Yunfan Hu (yunfanh2)
yunfanh2@illinois.edu
