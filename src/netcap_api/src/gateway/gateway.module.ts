import { Module } from '@nestjs/common';
import { LiveDataGateway } from './gateway';
import { PingPongModule } from 'src/pingPong.module';
import { PingPongService } from 'src/pingPong.service';

@Module({
    imports: [PingPongModule],
    providers: [LiveDataGateway],
})
export class GatewayModule {}
