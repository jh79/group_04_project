import { OnModuleInit } from '@nestjs/common';
import {
    MessageBody,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { PingPongService } from 'src/pingPong.service';
import { Server } from 'socket.io';
import { RealDataRequestBody } from 'src/types/RealDataRequestBody';

@WebSocketGateway({ cors: true })
export class LiveDataGateway implements OnModuleInit {
    constructor(private readonly pingPongService: PingPongService) {}

    @WebSocketServer()
    server: Server;

    chartIntervals = new Map<number, NodeJS.Timer>();

    currentRequestBody: RealDataRequestBody[];

    onModuleInit() {
        this.server.on('connection', (socket) => {
            console.log(socket.id, 'connected');
            socket.on('disconnect', () => {
                console.log(socket.id, 'disconnected');
            });
        });
    }

    @SubscribeMessage('clientRequest')
    async onClientRequest(@MessageBody() body: RealDataRequestBody[]) {
        if (this.isDuplicateRequest(body)) {
            console.log('duplicate!');
            return;
        }
        this.removeStaleIntervals(body, this.currentRequestBody);
        this.currentRequestBody = body;
        for (const data of body) {
            this.analyze(data);
        }
    }

    async analyze(body: RealDataRequestBody) {
        switch (body.type) {
            case 'running_avg':
                this.average(body);
                break;
            case 'running_avg_component_gateway':
                this.average_component_gateway(body);
                break;
            case 'running_avg_component_ome':
                this.average_component_ome(body);
                break;
            case 'running_avg_component_ticker':
                this.average_component_ticker(body);
                break;
            case 'running_avg_component_market_data':
                this.average_component_market_data(body);
                break;
            case 'running_avg_component_order_life_cycle':
                this.average_component_order_life_cycle(body);
                break;
            case 'running_avg_component_public':
                this.average_component_public(body);
                break;
            case 'running_avg_component_private':
                this.average_component_private(body);
                break;
        }
    }

    isDuplicateRequest(requestData: RealDataRequestBody[]): boolean {
        if (!this.currentRequestBody) return false;
        if (!requestData) return true;
        if (requestData.length != this.currentRequestBody.length) return false;
        return !requestData.filter(
            (data, i) =>
                !this.isDataBodyEquivalent(this.currentRequestBody[i], data),
        ).length;
    }

    removeStaleIntervals(
        cur: RealDataRequestBody[],
        orig: RealDataRequestBody[],
    ) {
        const removedData = orig
            ? cur
                ? orig.filter(
                      (dataBody) =>
                          !cur.filter((curDataBody) =>
                              this.isDataBodyEquivalent(dataBody, curDataBody),
                          ).length,
                  )
                : orig
            : [];
        const removedIds = removedData.map((data) => data.chart_id);

        for (const id of removedIds) {
            console.log(`Stopping interval for chart ${id}`);
            clearInterval(this.chartIntervals.get(id));
            this.chartIntervals.delete(id);
        }
    }

    isDataBodyEquivalent(
        first: RealDataRequestBody,
        second: RealDataRequestBody,
    ) {
        const firstKeys = Object.keys(first);
        const secondKeys = Object.keys(second);
        for (const key of firstKeys) {
            if (first[key] != second[key]) return false;
        }

        for (const key of secondKeys) {
            if (first[key] != second[key]) return false;
        }

        return true;
    }

    async average(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }
        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result = await this.pingPongService.avgPingPongLive(
                            time_gte,
                        );
                        res = avg_result;
                    } else {
                        avg_result = await this.pingPongService.avgPingPong(
                            time_gte,
                        );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_gateway(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentGatewayLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentGateway(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentGatewayGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentGatewayGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_ome(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentOmeLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentOme(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentOmeGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentOmeGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_ticker(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentTickerLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentTicker(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentTickerGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentTickerGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_market_data(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentMarketDataLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentMarketData(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentMarketDataGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentMarketDataGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_order_life_cycle(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentLifeCycleLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentLifeCycle(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentLifeCycleGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentLifeCycleGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_public(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPublicLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPublic(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPublicGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPublicGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }

    async average_component_private(body: RealDataRequestBody) {
        this.chartIntervals.get(body.chart_id) &&
            clearInterval(this.chartIntervals.get(body.chart_id));
        if (body.stop) {
            console.log(`Stopping interval for chart ${body.chart_id}`);
            return;
        }

        this.chartIntervals.set(
            body.chart_id,
            setInterval(async () => {
                const time = Math.floor(Date.now() / 1000);
                const time_gte = time - body.interval;
                let avg_result;

                let res = {};
                if (body.group_by == 'None') {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPrivateLive(
                                time_gte,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPrivate(
                                time_gte,
                            );
                        res = avg_result;
                    }
                } else {
                    if (body.interval == 0) {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPrivateGroupByLive(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    } else {
                        avg_result =
                            await this.pingPongService.avgPingPongComponentPrivateGroupBy(
                                time_gte,
                                body.group_by,
                            );
                        res = avg_result;
                    }
                }

                console.log(
                    `\nMessage sent to Chart ${body.chart_id}\n`,
                    JSON.stringify(res, null, 2),
                );
                this.server.emit(body.chart_id.toString(), res);
            }, body.refresh_rate),
        );
    }
}
