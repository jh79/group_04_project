import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { Prisma, RawPCAP } from '@prisma/client';
import { OrderPackets } from './types/RawPCAP';

@Injectable()
export class RawPCAPService {
    constructor(private prisma: PrismaService) {}

    async node(
        rawPCAPWhereUniqueInput: Prisma.RawPCAPWhereUniqueInput,
    ): Promise<RawPCAP | null> {
        return this.prisma.rawPCAP.findUnique({
            where: rawPCAPWhereUniqueInput,
        });
    }

    async createRawPCAP(data: Prisma.RawPCAPCreateInput): Promise<RawPCAP> {
        return this.prisma.rawPCAP.create({
            data: {
                timestamp_sec: data.timestamp_sec,
                timestamp_nano: data.timestamp_nano,
                capture_length: data.capture_length,
                total_length: data.total_length,
                raw_data: data.raw_data,
            },
        });
    }

    async deleteRawPCAP(
        where: Prisma.RawPCAPWhereUniqueInput,
    ): Promise<RawPCAP> {
        return this.prisma.rawPCAP.delete({
            where,
        });
    }

    async getUniqueClOrdId(): Promise<string[]> {
        return (
            (await this.prisma.$queryRaw`
            SELECT DISTINCT \`raw_data\` ->> '$.fix_cl0rdid' as order_id 
            FROM RawPCAP 
            WHERE \`raw_data\` ->> '$.fix_checksum' IS NOT NULL
            ORDER BY \`raw_data\` ->> '$.fix_cl0rdid'
        `) as any[]
        ).map((clOrdId) => clOrdId.order_id);
    }

    async updateOrderDataRequests(): Promise<any> {
        const orderPackets = (await this.prisma.$queryRaw`
            SELECT 
              * 
            FROM 
              (
                SELECT 
                  \`raw_data\` ->> '$.fix_cl0rdid' as order_id, 
                  JSON_ARRAYAGG(
                    JSON_OBJECT(
                      'id', 
                      id, 
                      'tstamp_sec', 
                      timestamp_sec, 
                      'tstamp_nano', 
                      timestamp_nano,
                      'sender', 
                      \`raw_data\` ->> '$.fix_sender_comp_id',
                      'target', 
                      \`raw_data\` ->> '$.fix_target_comp_id',
                      'fix_type', 
                      CASE \`raw_data\` ->> '$.fix_msg_type' WHEN 'D' THEN 'ORDER' WHEN 'V' THEN 'MKT_REQ' WHEN '8' THEN 'EXEC_ORD' WHEN 'W' THEN 'MKT_RES' END, 
                      'msg_state', 
                      CASE WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 0 THEN 'GW_IN' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 1 THEN 'ACK_OUT' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 2 
                      AND \`raw_data\` ->> '$.fix_mdreq_id' IS NOT NULL THEN 'DRPCPY_IN' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 2 
                      AND \`raw_data\` ->> '$.fix_mdreq_id' IS NULL THEN 'OME_IN' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 3 THEN 'TICKER_IN' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 4 
                      AND \`raw_data\` ->> '$.ip_protocol' = '06' THEN 'PRIV_OUT' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 4 
                      AND \`raw_data\` ->> '$.ip_protocol' = '11' THEN 'PUBL_OUT' WHEN MOD(
                        \`raw_data\` ->> '$.fix_msg_seq_num', 
                        10
                      ) = 5 THEN 'PRIV_OUT' END
                    )
                  ) AS order_packets 
                FROM 
                  RawPCAP 
                WHERE 
                  \`raw_data\` ->> '$.fix_checksum' IS NOT NULL 
                GROUP BY 
                  \`raw_data\` ->> '$.fix_cl0rdid' 
                ORDER BY 
                  \`raw_data\` ->> '$.fix_cl0rdid'
              ) AS orders 
            WHERE 
              order_id NOT IN (
                SELECT 
                  order_id 
                FROM 
                  OrderRequests
              ) 
              AND order_id NOT IN (
                SELECT 
                  order_id 
                FROM 
                  DataRequests
              )

        `) as OrderPackets[];

        const newOrdersRequests = orderPackets
            .filter((packets) => packets.order_id.substring(0, 1) === 'O')
            .map((packets): Prisma.OrderRequestsCreateInput => {
                let order: Prisma.OrderRequestsCreateInput = {
                    order_id: '',
                    source: '',
                    destination: '',
                    gateway_in_sec: 0,
                    gateway_in_nano: 0,
                    ack_tstamp_sec: 0,
                    ack_tstamp_nano: 0,
                    ome_in_sec: 0,
                    ome_in_nano: 0,
                    ticker_in_sec: 0,
                    ticker_in_nano: 0,
                    public_out_sec: 0,
                    public_out_nano: 0,
                    private_out_sec: 0,
                    private_out_nano: 0,
                };

                order.order_id = packets.order_id;
                const traderRegex = /TRADER\d+/;
                const exchangeRegex = /EXCHANGE\d+/;

                for (const packet of packets.order_packets) {
                    if (!order.source) {
                        if (traderRegex.test(packet.sender))
                            order.source = packet.sender;
                        if (traderRegex.test(packet.target))
                            order.source = packet.target;
                    }
                    if (!order.destination) {
                        if (exchangeRegex.test(packet.target))
                            order.destination = packet.target;
                        if (exchangeRegex.test(packet.sender))
                            order.destination = packet.sender;
                    }

                    switch (packet.msg_state) {
                        case 'GW_IN':
                            order.gateway_in_sec = packet.tstamp_sec;
                            order.gateway_in_nano = packet.tstamp_nano;
                            break;
                        case 'ACK_OUT':
                            order.ack_tstamp_sec = packet.tstamp_sec;
                            order.ack_tstamp_nano = packet.tstamp_nano;
                            break;
                        case 'OME_IN':
                            order.ome_in_sec = packet.tstamp_sec;
                            order.ome_in_nano = packet.tstamp_nano;
                            break;
                        case 'TICKER_IN':
                            order.ticker_in_sec = packet.tstamp_sec;
                            order.ticker_in_nano = packet.tstamp_nano;
                            break;
                        case 'PUBL_OUT':
                            order.public_out_sec = packet.tstamp_sec;
                            order.public_out_nano = packet.tstamp_nano;
                            break;
                        case 'PRIV_OUT':
                            order.private_out_sec = packet.tstamp_sec;
                            order.private_out_nano = packet.tstamp_nano;
                            break;
                    }
                }
                return order;
            })
            .filter((order) => {
                for (const key in order) {
                    if (!order[key]) {
                        console.log('INCOMPLETE ORDER SKIPPING...');
                        return false;
                    }
                }
                return true;
            });

        const newDataRequests = orderPackets
            .filter((packets) => packets.order_id.substring(0, 1) === 'M')
            .map((packets): Prisma.DataRequestsCreateInput => {
                let dataReq: Prisma.DataRequestsCreateInput = {
                    order_id: '',
                    source: '',
                    destination: '',
                    gateway_in_sec: 0,
                    gateway_in_nano: 0,
                    ack_tstamp_sec: 0,
                    ack_tstamp_nano: 0,
                    dropcopy_in_sec: 0,
                    dropcopy_in_nano: 0,
                    private_out_sec: 0,
                    private_out_nano: 0,
                };

                dataReq.order_id = packets.order_id;
                const traderRegex = /TRADER\d+/;
                const exchangeRegex = /EXCHANGE\d+/;

                for (const packet of packets.order_packets) {
                    if (!dataReq.source) {
                        if (traderRegex.test(packet.sender))
                            dataReq.source = packet.sender;
                        if (traderRegex.test(packet.target))
                            dataReq.source = packet.target;
                    }
                    if (!dataReq.destination) {
                        if (exchangeRegex.test(packet.target))
                            dataReq.destination = packet.target;
                        if (exchangeRegex.test(packet.sender))
                            dataReq.destination = packet.sender;
                    }

                    switch (packet.msg_state) {
                        case 'GW_IN':
                            dataReq.gateway_in_sec = packet.tstamp_sec;
                            dataReq.gateway_in_nano = packet.tstamp_nano;
                            break;
                        case 'ACK_OUT':
                            dataReq.ack_tstamp_sec = packet.tstamp_sec;
                            dataReq.ack_tstamp_nano = packet.tstamp_nano;
                            break;
                        case 'DRPCPY_IN':
                            dataReq.dropcopy_in_sec = packet.tstamp_sec;
                            dataReq.dropcopy_in_nano = packet.tstamp_nano;
                            break;
                        case 'PRIV_OUT':
                            dataReq.private_out_sec = packet.tstamp_sec;
                            dataReq.private_out_nano = packet.tstamp_nano;
                            break;
                    }
                }
                return dataReq;
            })
            .filter((dataReq) => {
                for (const key in dataReq) {
                    if (!dataReq[key]) {
                        console.log(
                            'INCOMPLETE MARKET DATA REQUEST SKIPPING...',
                        );
                        return false;
                    }
                }
                return true;
            });

        let ret = {
            orderChanges: 0,
            dataRequestChanges: 0,
        };

        try {
            const orderChanges = await this.prisma.orderRequests.createMany({
                data: newOrdersRequests,
            });

            const dataReqChanges = await this.prisma.dataRequests.createMany({
                data: newDataRequests,
            });

            ret = {
                orderChanges: orderChanges.count,
                dataRequestChanges: dataReqChanges.count,
            };
        } catch (error) {
            console.log('Error occurred when creating records, skipping...');
        }
        return ret;
    }
}
