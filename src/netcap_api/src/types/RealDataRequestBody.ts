import { interval } from 'rxjs';

export type RealDataRequestBody = {
    chart_id: number;
    stop: boolean;
    type: string;
    interval: number;
    refresh_rate: number;
    group_by: string;
};
