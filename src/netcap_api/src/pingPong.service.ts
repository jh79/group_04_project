import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { Prisma, PingPong } from '@prisma/client';
import { map } from 'rxjs';

@Injectable()
export class PingPongService {
    constructor(private prisma: PrismaService) {}

    async pingPong(
        pingPongWhereUniqueInput: Prisma.PingPongWhereUniqueInput,
    ): Promise<PingPong | null> {
        return this.prisma.pingPong.findUnique({
            where: pingPongWhereUniqueInput,
        });
    }

    async getAllPingPongs(): Promise<PingPong[]> {
        return this.prisma.pingPong.findMany();
    }

    async pingPongs(params: {
        skip?: number;
        take?: number;
        cursor?: Prisma.PingPongWhereUniqueInput;
        where?: Prisma.PingPongWhereInput;
        orderBy?: Prisma.PingPongOrderByWithRelationInput;
    }): Promise<PingPong[]> {
        const { skip, take, cursor, where, orderBy } = params;
        return this.prisma.pingPong.findMany({
            skip,
            take,
            cursor,
            where,
            orderBy,
        });
    }

    async getPingPongCount(): Promise<number> {
        return this.prisma.pingPong.count();
    }

    async createPingPong(data: Prisma.PingPongCreateInput): Promise<PingPong> {
        return this.prisma.pingPong.create({
            data: {
                order_id: data.order_id,
                source_ip: data.source_ip,
                gateway_ip: data.gateway_ip,
                trader_in_sec: data.trader_in_sec,
                trader_in_nano: data.trader_in_nano,
                trader_out_sec: data.trader_out_sec,
                trader_out_nano: data.trader_out_nano,
                gateway_in_sec: data.gateway_in_sec,
                gateway_in_nano: data.gateway_in_nano,
                gateway_out_sec: data.gateway_out_sec,
                gateway_out_nano: data.gateway_out_nano,
                ome_in_sec: data.ome_in_sec,
                ome_in_nano: data.ome_in_nano,
                ome_out_sec: data.ome_out_sec,
                ome_out_nano: data.ome_out_nano,
                ticker_out_sec: data.ticker_out_sec,
                ticker_out_nano: data.ticker_out_nano,
                latency: data.latency,
            },
        });
    }

    async updatePingPong(params: {
        where: Prisma.PingPongWhereUniqueInput;
        data: Prisma.PingPongUpdateInput;
    }): Promise<PingPong> {
        const { where, data } = params;
        return this.prisma.pingPong.update({
            data,
            where,
        });
    }

    async deletePingPong(
        where: Prisma.PingPongWhereUniqueInput,
    ): Promise<PingPong> {
        return this.prisma.pingPong.delete({
            where,
        });
    }

    prev_avgPingPong = {
        time: 0,
        latency: 0,
    };

    async avgPingPong(time: number): Promise<any> {
        const time1 = 1682877199;
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    gte: time,
                },
            },
            _avg: {
                latency: true,
            },
        });

        console.log(avg);

        if (avg._avg.latency == null) {
            this.prev_avgPingPong['time'] = time;
            return this.prev_avgPingPong;
        }

        let res = {
            time: time,
            latency: avg._avg.latency,
        };

        this.prev_avgPingPong = res;

        return res;
    }

    async avgPingPongLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPong['time'] = time;
            return this.prev_avgPingPong;
        }

        let res = {
            time: time,
            latency: avg[0].latency,
        };

        this.prev_avgPingPong = res;

        return res;
    }

    prev_avgPingPongGroupBy = {};

    async avgPingPongGroupBy(time: number, group: string): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                latency: true,
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongGroupBy['time'] = time;
            return this.prev_avgPingPongGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            map[avg[i][groupByField as any]] = avg[i]._avg.latency;
        }

        this.prev_avgPingPongGroupBy = map;

        return map;
    }

    async avgPingPongGroupByLive(time: number, group: string): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongGroupBy['time'] = time;
            return this.prev_avgPingPongGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            map[avg[i][groupByField as any]] = avg[i].latency;
        }

        this.prev_avgPingPongGroupBy = map;

        return map;
    }

    prev_avgPingPongComponentGateway = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentGateway(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                ome_in_sec: true,
                ome_in_nano: true,
            },
        });

        if (avg._avg.ome_in_sec == null) {
            this.prev_avgPingPongComponentGateway['time'] = time;
            return this.prev_avgPingPongComponentGateway;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.ome_in_sec,
                avg._avg.ome_in_nano,
                avg._avg.gateway_in_sec,
                avg._avg.gateway_in_nano,
            ),
        };

        this.prev_avgPingPongComponentGateway = res;

        return res;
    }

    async avgPingPongComponentGatewayLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentGateway['time'] = time;
            return this.prev_avgPingPongComponentGateway;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].ome_in_sec,
                avg[0].ome_in_nano,
                avg[0].gateway_in_sec,
                avg[0].gateway_in_nano,
            ),
        };

        this.prev_avgPingPongComponentGateway = res;

        return res;
    }

    prev_avgPingPongComponentOme = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentOme(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ome_in_sec: true,
                ome_in_nano: true,
                ome_out_sec: true,
                ome_out_nano: true,
            },
        });

        if (avg._avg.ome_out_sec == null) {
            this.prev_avgPingPongComponentOme['time'] = time;
            return this.prev_avgPingPongComponentOme;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.ome_out_sec,
                avg._avg.ome_out_nano,
                avg._avg.ome_in_sec,
                avg._avg.ome_in_nano,
            ),
        };

        this.prev_avgPingPongComponentOme = res;

        return res;
    }

    async avgPingPongComponentOmeLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentOme['time'] = time;
            return this.prev_avgPingPongComponentOme;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].ome_out_sec,
                avg[0].ome_out_nano,
                avg[0].ome_in_sec,
                avg[0].ome_in_nano,
            ),
        };

        this.prev_avgPingPongComponentOme = res;

        return res;
    }

    prev_avgPingPongComponentTicker = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentTicker(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ome_out_sec: true,
                ome_out_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
        });
        console.log(avg);
        if (avg._avg.ticker_out_sec == null) {
            this.prev_avgPingPongComponentTicker['time'] = time;
            return this.prev_avgPingPongComponentTicker;
        }
        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.ticker_out_sec,
                avg._avg.ticker_out_nano,
                avg._avg.ome_out_sec,
                avg._avg.ome_out_nano,
            ),
        };

        this.prev_avgPingPongComponentTicker = res;

        return res;
    }

    async avgPingPongComponentTickerLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentTicker['time'] = time;
            return this.prev_avgPingPongComponentTicker;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].ticker_out_sec,
                avg[0].ticker_out_nano,
                avg[0].ome_out_sec,
                avg[0].ome_out_nano,
            ),
        };

        this.prev_avgPingPongComponentTicker = res;

        return res;
    }

    prev_avgPingPongComponenGatewaytGroupBy = {};

    async avgPingPongComponentGatewayGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                ome_in_sec: true,
                ome_in_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenGatewaytGroupBy['time'] = time;
            return this.prev_avgPingPongComponenGatewaytGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.ome_in_sec,
                avg[i]._avg.ome_in_nano,
                avg[i]._avg.gateway_in_sec,
                avg[i]._avg.gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenGatewaytGroupBy = map;

        return map;
    }

    async avgPingPongComponentGatewayGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenGatewaytGroupBy['time'] = time;
            return this.prev_avgPingPongComponenGatewaytGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].ome_in_sec,
                avg[i].ome_in_nano,
                avg[i].gateway_in_sec,
                avg[i].gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenGatewaytGroupBy = map;

        return map;
    }

    prev_avgPingPongComponenOmeGroupBy = {};
    async avgPingPongComponentOmeGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ome_in_sec: true,
                ome_in_nano: true,
                ome_out_sec: true,
                ome_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenOmeGroupBy['time'] = time;
            return this.prev_avgPingPongComponenOmeGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.ome_out_sec,
                avg[i]._avg.ome_out_nano,
                avg[i]._avg.ome_in_sec,
                avg[i]._avg.ome_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenOmeGroupBy = map;

        return map;
    }

    async avgPingPongComponentOmeGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenOmeGroupBy['time'] = time;
            return this.prev_avgPingPongComponenOmeGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].ome_out_sec,
                avg[i].ome_out_nano,
                avg[i].ome_in_sec,
                avg[i].ome_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenOmeGroupBy = map;

        return map;
    }

    prev_avgPingPongComponenTicketGroupBy = {};

    async avgPingPongComponentTickerGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ome_out_sec: true,
                ome_out_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenTicketGroupBy['time'] = time;
            return this.prev_avgPingPongComponenTicketGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.ticker_out_sec,
                avg[i]._avg.ticker_out_nano,
                avg[i]._avg.ome_out_sec,
                avg[i]._avg.ome_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenTicketGroupBy = map;

        return map;
    }

    async avgPingPongComponentTickerGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenTicketGroupBy['time'] = time;
            return this.prev_avgPingPongComponenTicketGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].ticker_out_sec,
                avg[i].ticker_out_nano,
                avg[i].ome_out_sec,
                avg[i].ome_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenTicketGroupBy = map;

        return map;
    }

    prev_avgPingPongComponentMarketData = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentMarketData(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ticker_out_sec: true,
                ticker_out_nano: true,
                gateway_out_sec: true,
                gateway_out_nano: true,
            },
        });

        if (avg._avg.ticker_out_sec == null) {
            this.prev_avgPingPongComponentMarketData['time'] = time;
            return this.prev_avgPingPongComponentMarketData;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.gateway_out_sec,
                avg._avg.gateway_out_nano,
                avg._avg.ticker_out_sec,
                avg._avg.ticker_out_nano,
            ),
        };

        this.prev_avgPingPongComponentMarketData = res;

        return res;
    }

    async avgPingPongComponentMarketDataLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentMarketData['time'] = time;
            return this.prev_avgPingPongComponentMarketData;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].gateway_out_sec,
                avg[0].gateway_out_nano,
                avg[0].ticker_out_sec,
                avg[0].ticker_out_nano,
            ),
        };

        this.prev_avgPingPongComponentMarketData = res;

        return res;
    }

    prev_avgPingPongComponentLifeCycle = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentLifeCycle(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                trader_out_sec: true,
                trader_out_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
        });

        if (avg._avg.ticker_out_sec == null) {
            this.prev_avgPingPongComponentLifeCycle['time'] = time;
            return this.prev_avgPingPongComponentLifeCycle;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.ticker_out_sec,
                avg._avg.ticker_out_nano,
                avg._avg.trader_out_sec,
                avg._avg.trader_out_nano,
            ),
        };

        if (res == null) {
            return this.prev_avgPingPongComponentLifeCycle;
        }

        this.prev_avgPingPongComponentLifeCycle = res;

        return res;
    }

    async avgPingPongComponentLifeCycleLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentLifeCycle['time'] = time;
            return this.prev_avgPingPongComponentLifeCycle;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].ticker_out_sec,
                avg[0].ticker_out_nano,
                avg[0].trader_out_sec,
                avg[0].trader_out_nano,
            ),
        };

        this.prev_avgPingPongComponentLifeCycle = res;

        return res;
    }

    prev_avgPingPongComponenMarketDataGroupBy = {};
    async avgPingPongComponentMarketDataGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                ticker_out_sec: true,
                ticker_out_nano: true,
                gateway_out_sec: true,
                gateway_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenMarketDataGroupBy['time'] = time;
            return this.prev_avgPingPongComponenMarketDataGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.gateway_out_sec,
                avg[i]._avg.gateway_out_nano,
                avg[i]._avg.ticker_out_sec,
                avg[i]._avg.ticker_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenMarketDataGroupBy = map;

        return map;
    }

    async avgPingPongComponentMarketDataGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenMarketDataGroupBy['time'] = time;
            return this.prev_avgPingPongComponenMarketDataGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].gateway_out_sec,
                avg[i].gateway_out_nano,
                avg[i].ticker_out_sec,
                avg[i].ticker_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenMarketDataGroupBy = map;

        return map;
    }

    prev_avgPingPongComponenLifeCycleGroupBy = {};

    async avgPingPongComponentLifeCycleGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                trader_out_sec: true,
                trader_out_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenLifeCycleGroupBy['time'] = time;
            return this.prev_avgPingPongComponenLifeCycleGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.ticker_out_sec,
                avg[i]._avg.ticker_out_nano,
                avg[i]._avg.trader_out_sec,
                avg[i]._avg.trader_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenLifeCycleGroupBy = map;

        return map;
    }

    async avgPingPongComponentLifeCycleGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenLifeCycleGroupBy['time'] = time;
            return this.prev_avgPingPongComponenLifeCycleGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].ticker_out_sec,
                avg[i].ticker_out_nano,
                avg[i].trader_out_sec,
                avg[i].trader_out_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenLifeCycleGroupBy = map;

        return map;
    }

    prev_avgPingPongComponentPublic = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentPublic(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
        });

        if (avg._avg.ticker_out_sec == null) {
            this.prev_avgPingPongComponentPublic['time'] = time;
            return this.prev_avgPingPongComponentPublic;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.ticker_out_sec,
                avg._avg.ticker_out_nano,
                avg._avg.gateway_in_sec,
                avg._avg.gateway_in_nano,
            ),
        };

        if (res == null) {
            return this.prev_avgPingPongComponentPublic;
        }

        this.prev_avgPingPongComponentPublic = res;

        return res;
    }

    async avgPingPongComponentPublicLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentPublic['time'] = time;
            return this.prev_avgPingPongComponentPublic;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].ticker_out_sec,
                avg[0].ticker_out_nano,
                avg[0].gateway_in_sec,
                avg[0].gateway_in_nano,
            ),
        };

        this.prev_avgPingPongComponentPublic = res;

        return res;
    }

    prev_avgPingPongComponenPublicGroupBy = {};

    async avgPingPongComponentPublicGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                ticker_out_sec: true,
                ticker_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenPublicGroupBy['time'] = time;
            return this.prev_avgPingPongComponenPublicGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.ticker_out_sec,
                avg[i]._avg.ticker_out_nano,
                avg[i]._avg.gateway_in_sec,
                avg[i]._avg.gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenPublicGroupBy = map;

        return map;
    }

    async avgPingPongComponentPublicGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenPublicGroupBy['time'] = time;
            return this.prev_avgPingPongComponenPublicGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].ticker_out_sec,
                avg[i].ticker_out_nano,
                avg[i].gateway_in_sec,
                avg[i].gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenPublicGroupBy = map;

        return map;
    }

    prev_avgPingPongComponentPrivate = {
        time: 0,
        latency: 0,
    };

    async avgPingPongComponentPrivate(time: number): Promise<any> {
        let avg = await this.prisma.pingPong.aggregate({
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                gateway_out_sec: true,
                gateway_out_nano: true,
            },
        });

        if (avg._avg.gateway_out_nano == null) {
            this.prev_avgPingPongComponentPrivate['time'] = time;
            return this.prev_avgPingPongComponentPrivate;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg._avg.gateway_out_sec,
                avg._avg.gateway_out_nano,
                avg._avg.gateway_in_sec,
                avg._avg.gateway_in_nano,
            ),
        };

        if (res == null) {
            return this.prev_avgPingPongComponentPrivate;
        }

        this.prev_avgPingPongComponentPrivate = res;

        return res;
    }

    async avgPingPongComponentPrivateLive(time: number): Promise<any> {
        const avg = await this.prisma.pingPong.findMany({
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
            take: 1,
        });

        console.log(avg);

        if (avg[0] == null) {
            this.prev_avgPingPongComponentPrivate['time'] = time;
            return this.prev_avgPingPongComponentPrivate;
        }

        let res = {
            time: time,
            latency: this.calculateDiffAvg(
                avg[0].gateway_out_sec,
                avg[0].gateway_out_nano,
                avg[0].gateway_in_sec,
                avg[0].gateway_in_nano,
            ),
        };

        this.prev_avgPingPongComponentPrivate = res;

        return res;
    }

    prev_avgPingPongComponenPrivateGroupBy = {};

    async avgPingPongComponentPrivateGroupBy(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        let avg = await this.prisma.pingPong.groupBy({
            by: [groupByField as any],
            where: {
                trader_in_sec: {
                    //It may shows null because we have no data in the db
                    //gte: 1682877199,
                    gte: time,
                },
            },
            _avg: {
                gateway_in_sec: true,
                gateway_in_nano: true,
                gateway_out_sec: true,
                gateway_out_nano: true,
            },
            orderBy: {
                [groupByField]: 'asc',
            },
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenPrivateGroupBy['time'] = time;
            return this.prev_avgPingPongComponenPrivateGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i]._avg.gateway_out_sec,
                avg[i]._avg.gateway_out_nano,
                avg[i]._avg.gateway_in_sec,
                avg[i]._avg.gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenPrivateGroupBy = map;

        return map;
    }

    async avgPingPongComponentPrivateGroupByLive(
        time: number,
        group: string,
    ): Promise<any> {
        const groupByField = group as keyof Prisma.PingPongScalarFieldEnum;
        const avg = await this.prisma.pingPong.findMany({
            distinct: [groupByField as any],
            orderBy: [
                {
                    trader_in_sec: 'desc',
                },
                {
                    trader_in_nano: 'desc',
                },
            ],
        });

        if (avg.length == 0) {
            this.prev_avgPingPongComponenPrivateGroupBy['time'] = time;
            return this.prev_avgPingPongComponenPrivateGroupBy;
        }

        let map = {};

        map['time'] = time.toString();

        for (let i = 0; i < avg.length; i++) {
            const lantency = this.calculateDiffAvg(
                avg[i].gateway_out_sec,
                avg[i].gateway_out_nano,
                avg[i].gateway_in_sec,
                avg[i].gateway_in_nano,
            );
            map[avg[i][groupByField as any]] = lantency;
        }

        this.prev_avgPingPongComponenPrivateGroupBy = map;

        return map;
    }

    calculateDiffAvg(
        out_time_sec: any,
        out_time_nano: any,
        in_time_sec: any,
        in_time_nano: any,
    ) {
        const sec_dif = Number(out_time_sec - in_time_sec);
        const nano_dif = Number(out_time_nano - in_time_nano);
        return Math.abs(Number(nano_dif + sec_dif * 1000000000));
    }
}
