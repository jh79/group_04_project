import React from 'react';
import './App.css';
import AppLayout from './AppLayout';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

function App() {
    const queryClient = new QueryClient();
    return (
        <QueryClientProvider client={queryClient}>
            <AppLayout />
        </QueryClientProvider>
    );
}

export default App;
