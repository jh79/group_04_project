import {
    ColumnDef,
    flexRender,
    getCoreRowModel,
    useReactTable,
} from '@tanstack/react-table';
import { PCAPType } from '../../../types/PCAPTypes';
import dateFormat from 'dateformat';
import { useInfoSidebarContext } from '../../../providers/InfoSidebarProvider';

const TableCard = ({ PCAPData }: { PCAPData: PCAPType[] }) => {
    const convertNanoEpoch = (epoch: string) => {
        return new Date(Number(epoch.substring(0, 13)));
    };

    const [, setInfoSidebarState] = useInfoSidebarContext();

    const PCAPColumns: ColumnDef<PCAPType>[] = [
        {
            header: 'Packet ID',
            accessorKey: 'id',
        },
        {
            header: 'Trader IP',
            accessorKey: 'source_ip',
        },
        {
            header: 'Exchange IP',
            accessorKey: 'gateway_ip',
        },
        {
            header: 'Time Sent',
            accessorFn: (row) =>
                dateFormat(
                    convertNanoEpoch(
                        `${row.gateway_in_sec}${row.gateway_in_nano}`,
                    ),
                    'h:MM:ss',
                ),
        },
        {
            header: 'Time Processed',
            accessorFn: (row) =>
                dateFormat(
                    convertNanoEpoch(
                        `${row.gateway_out_sec}${row.gateway_out_nano}`,
                    ),
                    'h:MM:ss',
                ),
        },
        {
            header: 'Ack Latency (ns)',
            accessorKey: 'latency',
        },
        {
            header: 'Packet Details',
            accessorFn: (row) => {
                return row;
            },
            cell: ({ getValue }) => {
                return (
                    <button
                        className={
                            'rounded-md bg-indigo-50 px-2.5 py-1.5 text-sm font-semibold text-indigo-600 shadow-sm hover:bg-indigo-100'
                        }
                        onClick={() =>
                            setInfoSidebarState({
                                isOpen: true,
                                state: getValue() as PCAPType,
                            })
                        }
                    >
                        View Details
                    </button>
                );
            },
        },
    ];

    const PCAPTable = useReactTable({
        data: PCAPData,
        columns: PCAPColumns,
        getCoreRowModel: getCoreRowModel(),
    });

    if (PCAPData.length < 1) return <></>;
    // if (isFetching) return <p>Loading...</p>
    return (
        <div className="h-[calc(100vh-15rem)] overflow-y-auto scrollbar scrollbar-thin scrollbar-thumb-rounded-full scrollbar-track-rounded-full scrollbar-thumb-gray-400 scrollbar-track-gray-200">
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="sm:flex sm:items-center -mx-4">
                    <div className="sm:flex-auto">
                        <h1 className="text-base font-semibold leading-6 text-gray-900">
                            Network Traffic
                        </h1>
                        <p className="mt-2 text-sm text-gray-700">
                            A list of all the packets being sent on the
                            simulated trading network.
                        </p>
                    </div>
                </div>
                <div className="mt-8 flow-root">
                    <div className="-mx-4 -my-2">
                        <div className="inline-block min-w-full py-2 align-middle">
                            <table className="min-w-full border-separate border-spacing-0">
                                <thead>
                                    {PCAPTable.getHeaderGroups().map(
                                        (headerGroup) => (
                                            <tr key={headerGroup.id}>
                                                {headerGroup.headers.map(
                                                    (header) => (
                                                        <th
                                                            key={header.id}
                                                            scope="col"
                                                            className="sticky top-0 z-10 hidden border-b border-gray-300 bg-white bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter sm:table-cell"
                                                        >
                                                            {header.isPlaceholder
                                                                ? null
                                                                : flexRender(
                                                                      header
                                                                          .column
                                                                          .columnDef
                                                                          .header,
                                                                      header.getContext(),
                                                                  )}
                                                        </th>
                                                    ),
                                                )}
                                            </tr>
                                        ),
                                    )}
                                </thead>
                                <tbody className="divide-y divide-gray-200 bg-white">
                                    {PCAPTable.getRowModel().rows.map((row) => (
                                        <tr key={row.id}>
                                            {row
                                                .getVisibleCells()
                                                .map((cell) => (
                                                    <td
                                                        className="whitespace-nowrap px-3 py-4 text-sm text-gray-500"
                                                        key={cell.id}
                                                    >
                                                        {flexRender(
                                                            cell.column
                                                                .columnDef.cell,
                                                            cell.getContext(),
                                                        )}
                                                    </td>
                                                ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TableCard;
