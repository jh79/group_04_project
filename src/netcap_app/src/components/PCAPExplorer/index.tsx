import { useQuery } from '@tanstack/react-query';
import TableCard from './TableCard';
import TableFooter from './TableFooter';
import { PCAPType } from '../../types/PCAPTypes';
import { useMemo, useState } from 'react';
import { usePCAPPaginationContext } from '../../providers/PCAPPaginationProvider';
import InfoSidebar from './InfoSidebar';

const PCAPExplorer = () => {
    const [paginationState, setPaginationState] = usePCAPPaginationContext();
    const [totalResultCount, setTotalResultCount] = useState(0);

    const { data: PCAPData } = useQuery({
        queryKey: [
            'PCAPDataQuery',
            paginationState.pageSize,
            paginationState.pageIndex,
        ],
        queryFn: async ({ queryKey }) => {
            const [, pageSize, pageIndex] = queryKey;
            const res = await fetch(
                `http://${process.env.REACT_APP_BACKEND_IP
                }:3000/pingPongs?take=${pageSize}&skip=${(pageIndex as number) * (pageSize as number)
                }`,
            );
            const json = await res.json();
            setTotalResultCount(json.count);
            return json;
        },
        keepPreviousData: true,
    });

    const PCAPDataMemo = useMemo<PCAPType[]>(
        () => (PCAPData ? PCAPData.result : []),
        [PCAPData],
    );
    return (
        <>
            <InfoSidebar />
            <h1 className="text-3xl mb-5 font-semibold text-gray-700">
                Order Explorer
            </h1>
            <div className="rounded-xl p-5 bg-white shadow-md">
                <TableCard PCAPData={PCAPDataMemo} />
                <TableFooter
                    handleNext={() => {
                        if (
                            (paginationState.pageIndex + 1) *
                            paginationState.pageSize >=
                            totalResultCount
                        )
                            return;
                        setPaginationState((cur) => ({
                            pageIndex: cur.pageIndex + 1,
                            pageSize: paginationState.pageSize,
                        }));
                    }}
                    handlePrev={() => {
                        if (paginationState.pageIndex <= 0) return;
                        setPaginationState((cur) => ({
                            pageIndex: cur.pageIndex - 1,
                            pageSize: paginationState.pageSize,
                        }));
                    }}
                    totalResultCount={totalResultCount}
                />
            </div>
        </>
    );
};

export default PCAPExplorer;
