import { Fragment, useEffect } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { XMarkIcon } from '@heroicons/react/24/outline';
import { useInfoSidebarContext } from '../../../providers/InfoSidebarProvider';

const InfoSidebar = () => {
    const [infoSidebarState, setInfoSidebarState] = useInfoSidebarContext();
    const subtractStrings = (num1: string, num2: string): string => {
        // Convert the strings to arrays of digits and reverse them
        const digits1: number[] = Array.from(num1).map(Number).reverse();
        const digits2: number[] = Array.from(num2).map(Number).reverse();

        // Perform the subtraction
        const maxLength = Math.max(digits1.length, digits2.length);
        let carry = 0;
        const difference: number[] = [];

        for (let i = 0; i < maxLength; i++) {
            const digit1 = digits1[i] || 0;
            const digit2 = digits2[i] || 0;

            let digitDiff = digit1 - digit2 + carry;

            if (digitDiff < 0) {
                digitDiff += 10;
                carry = -1;
            } else {
                carry = 0;
            }

            difference.push(digitDiff);
        }

        // Remove leading zeros from the difference
        while (
            difference.length > 1 &&
            difference[difference.length - 1] === 0
        ) {
            difference.pop();
        }

        // Reverse the difference and convert it back to a string
        const result = difference.reverse().join('');

        return result;
    };

    const packetSent = `${infoSidebarState.state.trader_out_sec}${infoSidebarState.state.trader_out_nano}`;
    const packetAck = `${infoSidebarState.state.trader_in_sec}${infoSidebarState.state.trader_in_nano}`;
    const gatewayRecv = `${infoSidebarState.state.gateway_in_sec}${infoSidebarState.state.gateway_in_nano}`;
    const omeRecv = `${infoSidebarState.state.ome_in_sec}${infoSidebarState.state.ome_in_nano}`;
    const omeSent = `${infoSidebarState.state.ome_out_sec}${infoSidebarState.state.ome_out_nano}`;
    const tickerSent = `${infoSidebarState.state.ticker_out_sec}${infoSidebarState.state.ticker_out_nano}`;
    const gatewaySent = `${infoSidebarState.state.gateway_out_sec}${infoSidebarState.state.gateway_out_nano}`;

    return (
        <Transition.Root show={infoSidebarState.isOpen} as={Fragment}>
            <Dialog
                as="div"
                className="relative z-10"
                onClose={() => {
                    setInfoSidebarState((cur) => ({
                        isOpen: false,
                        state: cur.state,
                    }));
                }}
            >
                <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-500"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in-out duration-500"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-0 overflow-hidden">
                    <div className="absolute inset-0 overflow-hidden">
                        <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
                            <Transition.Child
                                as={Fragment}
                                enter="transform transition ease-in-out duration-500 sm:duration-700"
                                enterFrom="translate-x-full"
                                enterTo="translate-x-0"
                                leave="transform transition ease-in-out duration-500 sm:duration-700"
                                leaveFrom="translate-x-0"
                                leaveTo="translate-x-full"
                            >
                                <Dialog.Panel className="pointer-events-auto relative w-96">
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-in-out duration-500"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in-out duration-500"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="absolute left-0 top-0 -ml-8 flex pr-2 pt-4 sm:-ml-10 sm:pr-4">
                                            <button
                                                type="button"
                                                className="rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                                                onClick={() =>
                                                    setInfoSidebarState(
                                                        (cur) => ({
                                                            isOpen: false,
                                                            state: cur.state,
                                                        }),
                                                    )
                                                }
                                            >
                                                <span className="sr-only">
                                                    Close panel
                                                </span>
                                                <XMarkIcon
                                                    className="h-6 w-6"
                                                    aria-hidden="true"
                                                />
                                            </button>
                                        </div>
                                    </Transition.Child>
                                    <div className="h-full overflow-y-auto bg-white p-8">
                                        <div className="pb-8">
                                            <h3 className="font-medium text-gray-900">
                                                General Packet Information
                                            </h3>
                                            <dl className="mt-2 divide-y divide-gray-200 border-b border-t border-gray-200">
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Packet ID
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {
                                                            infoSidebarState
                                                                .state.id
                                                        }
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Order ID
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {
                                                            infoSidebarState
                                                                .state.order_id
                                                        }
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Trader IP
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {
                                                            infoSidebarState
                                                                .state.source_ip
                                                        }
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Exchange IP
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {
                                                            infoSidebarState
                                                                .state
                                                                .gateway_ip
                                                        }
                                                    </dd>
                                                </div>
                                            </dl>
                                        </div>
                                        <div className="pb-8">
                                            <h3 className="font-medium text-gray-900">
                                                Absolute Timing Information
                                            </h3>
                                            <dl className="mt-2 divide-y divide-gray-200 border-b border-t border-gray-200">
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Packet Sent
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {packetSent}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Packet Acknowledged
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {packetAck}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Gateway Received
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {gatewayRecv}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        OME Received
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {omeRecv}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        OME Processed
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {omeSent}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Public Datafeed Sent
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {tickerSent}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Private Datafeed Sent
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {gatewaySent}{' '}
                                                    </dd>
                                                </div>
                                            </dl>
                                        </div>
                                        <div className="pb-8">
                                            <h3 className="font-medium text-gray-900">
                                                Relative Timing Information
                                            </h3>
                                            <dl className="mt-2 divide-y divide-gray-200 border-b border-t border-gray-200">
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Packet Acknowledged
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            packetAck,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Gateway Received
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            gatewayRecv,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        OME Received
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            omeRecv,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        OME Processed
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            omeSent,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Public Datafeed Sent
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            tickerSent,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                                <div className="flex justify-between py-3 text-sm font-medium">
                                                    <dt className="text-gray-500">
                                                        Private Datafeed Sent
                                                    </dt>
                                                    <dd className="text-gray-900">
                                                        {subtractStrings(
                                                            gatewaySent,
                                                            packetSent,
                                                        )}{' '}
                                                    </dd>
                                                </div>
                                            </dl>
                                        </div>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    );
};

export default InfoSidebar;
