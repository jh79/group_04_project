import { useQuery } from '@tanstack/react-query';
import TitledDivider from '../../TitledDivider';
import NodeStatusCard from './NodeStatusCard';

type NetworkNode = {
    status: number;
    host_name: string;
    port: number;
    type: string;
    message_num: number;
};

const nodes = [
    {
        status: 1,
        host_name: '192.168.33.10',
        port: 3125,
        type: 'EXCHANGE',
        message_num: 312,
    },
    {
        status: 1,
        host_name: '192.168.33.10',
        port: 3126,
        type: 'EXCHANGE',
        message_num: 3432,
    },
    {
        status: 1,
        host_name: '192.168.33.10',
        port: 3127,
        type: 'EXCHANGE',
        message_num: 6724,
    },
    {
        status: 0,
        host_name: '192.168.33.11',
        port: 3125,
        type: 'TRADER',
        message_num: 73,
    },
    {
        status: 1,
        host_name: '192.168.33.12',
        port: 3125,
        type: 'TRADER',
        message_num: 623,
    },
    {
        status: 1,
        host_name: '192.168.33.13',
        port: 3125,
        type: 'TRADER',
        message_num: 37,
    },
    {
        status: 0,
        host_name: '192.168.33.14',
        port: 3125,
        type: 'TRADER',
        message_num: 2354,
    },
    {
        status: 0,
        host_name: '192.168.33.15',
        port: 3125,
        type: 'TRADER',
        message_num: 634,
    },
    {
        status: 0,
        host_name: '192.168.33.16',
        port: 3125,
        type: 'TRADER',
        message_num: 6243,
    },
];

const NetworkInfoCard = () => {
    const { data: nodesData } = useQuery({
        queryKey: ['Nodes'],
        queryFn: async () => {
            const res = await fetch(
                `http://${process.env.REACT_APP_BACKEND_IP}:3000/nodes`,
            );

            return await res.json() as NetworkNode[];
        },
    });

    console.log(nodesData);
    return (
        <div className="shadow-md w-auto rounded-lg bg-white mb-5 p-5">
            <TitledDivider title="Network Information" />
            <ul
                role="list"
                className="grid grid-cols-1 gap-x-6 gap-y-8 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 xl:gap-x-8"
            >
                {nodesData && nodesData.map((node) => (
                    <NodeStatusCard node={node} />
                ))}
            </ul>
        </div>
    );
};

export default NetworkInfoCard;
