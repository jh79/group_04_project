import NetworkInfoCard from './NetworkInfoCard';

const Dashboard = () => {
    return (
        <>
            <h1 className="text-3xl mb-5 font-semibold text-gray-700">
                Dashboard
            </h1>
            <NetworkInfoCard />
        </>
    );
};

export default Dashboard;
