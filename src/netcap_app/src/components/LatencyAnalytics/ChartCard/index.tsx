import {
    CartesianGrid,
    Legend,
    Line,
    LineChart,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis,
} from 'recharts';
import { PauseIcon, PlayIcon } from '@heroicons/react/20/solid';
import { useEffect, useState } from 'react';
import { useChartArrayContext } from '../../../providers/ChartArrayProvider';
import { ChartType } from '../../../types/ChartTypes';
import Select from './Select';
import { socket } from '../socket';
import dateFormat from 'dateformat';
import { Md5 } from 'ts-md5';

type SocketResponse =
    | {
          timestamp: number;
          latency: number;
      }
    | {
          timestamp: number;
          [key: string]: number;
      };

const ChartCard = ({ id }: { id: number }) => {
    const defaultChartState = {
        chart_id: id,
        type: 'running_avg',
        interval: 1000,
        refresh_rate: 1000,
        history_length: 20,
        group_by: 'None',
        stop: false,
    };

    const testSocketArray = Array.from(Array(50)).map((_, i) => ({
        time: i,
        'Trader 1': Math.random(),
        'Trader 2': Math.random(),
    }));

    const [socketArray, setSocketArray] = useState<SocketResponse[]>([]);
    const [xAxisKeys, setXAxisKeys] = useState<string[]>([]);

    const [chartArrayState, setChartArrayState] = useChartArrayContext();

    const [chartState, setChartState] = useState<ChartType>(defaultChartState);

    useEffect(() => {
        socket.on(`${id}`, (body: SocketResponse) => {
            // console.log(body);
            setXAxisKeys(Object.keys(body).filter((key) => key != 'time'));
            setSocketArray((cur) => [...cur, body]);
        });
        return () => {
            socket.off(`${id}`);
        };
    }, [socketArray]);

    useEffect(() => {
        setChartArrayState((cur) => cur.set(id, chartState));
        socket.emit(
            'clientRequest',
            Object.values(Object.fromEntries(chartArrayState)),
        );
    }, [chartState]);

    const typeOptions = [
        ['Ack Latency', 'running_avg'],
        ['Order Lifecycle', 'running_avg_component_order_life_cycle'],
        ['Gateway Latency', 'running_avg_component_gateway'],
        ['OME Latency', 'running_avg_component_ome'],
        ['Ticker Latency', 'running_avg_component_ticker'],
        ['Private Data', 'running_avg_component_private'],
        ['Public Data', 'running_avg_component_public'],
        ['Private vs Public', 'running_avg_component_market_data'],
    ];

    const pastelColors = [
        '#77DD77',
        '#836953',
        '#89cff0',
        '#99c5c4',
        '#9adedb',
        '#aa9499',
        '#aaf0d1',
        '#b2fba5',
    ];

    return (
        <div className="shadow-md w-auto rounded-lg bg-white p-5">
            <div className="relative mb-5">
                <div
                    className="absolute inset-0 flex items-center"
                    aria-hidden="true"
                >
                    <div className="w-full border-t border-gray-300" />
                </div>
                <div className="relative flex justify-start">
                    <span className="bg-white pr-3 text-base font-semibold leading-6 text-gray-900">
                        <select
                            id={'type'}
                            name={'type'}
                            className="bg-white mt-2 block w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            defaultValue={'Ack Latency'}
                            onChange={(e) => {
                                setSocketArray([]);
                                setChartState((cur) => ({
                                    chart_id: cur.chart_id,
                                    type: e.target.value,
                                    interval: cur.interval,
                                    refresh_rate: cur.refresh_rate,
                                    group_by: cur.group_by,
                                    history_length: cur.history_length,
                                    stop: cur.stop,
                                }));
                            }}
                        >
                            {typeOptions.map((opt) => (
                                <option
                                    className="bg-white"
                                    key={opt[1]}
                                    value={opt[1]}
                                >
                                    {opt[0]}
                                </option>
                            ))}
                        </select>
                    </span>
                </div>
            </div>
            <div className="h-[15rem]">
                <ResponsiveContainer>
                    <LineChart data={socketArray.slice(-50)}>
                        <CartesianGrid strokeDasharray={'3 3'} />
                        <YAxis
                            type="number"
                            tickFormatter={(value) =>
                                Number(value.toFixed(10)).toExponential()
                            }
                        />
                        <XAxis
                            tickFormatter={(value) => {
                                try {
                                    return dateFormat(
                                        new Date(Number(value) * 10 ** 3),
                                        'MM:ss',
                                    );
                                } catch {
                                    return '';
                                }
                            }}
                            dataKey={'time'}
                        />
                        <Tooltip />
                        <Legend />
                        {xAxisKeys.map((key, index) => (
                            <Line
                                type={'monotone'}
                                dataKey={key}
                                dot={false}
                                stroke={
                                    pastelColors[pastelColors.length % index]
                                }
                                isAnimationActive={false}
                            />
                        ))}
                    </LineChart>
                </ResponsiveContainer>
            </div>
            <nav className="flex items-center justify-between border-t border-gray-200 bg-white pt-4">
                <button
                    type="button"
                    className="rounded bg-indigo-50 px-4 py-4 text-sm font-semibold text-indigo-600 shadow-sm hover:bg-indigo-100"
                    onClick={() => {
                        setChartState((cur) => ({
                            chart_id: cur.chart_id,
                            type: cur.type,
                            interval: cur.interval,
                            refresh_rate: cur.refresh_rate,
                            group_by: cur.group_by,
                            history_length: cur.history_length,
                            stop: !cur.stop,
                        }));
                    }}
                >
                    {chartState.stop ? (
                        <PlayIcon className="h-4 w-4 text-blue-500" />
                    ) : (
                        <PauseIcon className="h-4 w-4 text-blue-300" />
                    )}
                </button>
                <div className="flex flex-row gap-x-2">
                    <Select
                        title="Refresh Rate"
                        options={[
                            ['0.5s', 500],
                            ['1s', 1000],
                            ['5s', 5000],
                            ['10s', 10000],
                        ]}
                        defaultVal={`${1000}`}
                        onChange={(e) => {
                            setChartState((cur) => ({
                                chart_id: cur.chart_id,
                                type: cur.type,
                                interval: cur.interval,
                                refresh_rate: +e.target.value,
                                group_by: cur.group_by,
                                history_length: cur.history_length,
                                stop: cur.stop,
                            }));
                        }}
                    />
                    <Select
                        title="Avg Depth"
                        options={[
                            ['Live', 0],
                            ['1s', 1000],
                            ['5s', 5000],
                            ['10s', 10000],
                            ['15s', 15000],
                            ['30s', 30000],
                            ['60s', 60000],
                        ]}
                        defaultVal={`${5000}`}
                        onChange={(e) => {
                            setSocketArray([]);
                            setChartState((cur) => ({
                                chart_id: cur.chart_id,
                                type: cur.type,
                                interval: +e.target.value,
                                refresh_rate: cur.refresh_rate,
                                group_by: cur.group_by,
                                history_length: cur.history_length,
                                stop: cur.stop,
                            }));
                        }}
                    />
                    <Select
                        title="Group By"
                        options={[
                            ['None', 'None'],
                            ['Trader', 'source_ip'],
                            ['Exchange', 'gateway_ip'],
                        ]}
                        defaultVal="None"
                        onChange={(e) => {
                            setSocketArray([]);
                            setChartState((cur) => ({
                                chart_id: cur.chart_id,
                                type: cur.type,
                                interval: cur.interval,
                                refresh_rate: cur.refresh_rate,
                                group_by: e.target.value,
                                history_length: cur.history_length,
                                stop: cur.stop,
                            }));
                        }}
                    />
                </div>
            </nav>
        </div>
    );
};

export default ChartCard;
