import { useEffect, useState } from 'react';
import { socket } from './socket';
import ChartCard from './ChartCard';

const LatencyAnalytics = () => {
    const [testSocketArray, setSocketArray] = useState<any[]>([]);

    useEffect(() => {
        socket.connect();
        return () => {
            socket.disconnect();
        };
    }, []);

    useEffect(() => {
        socket.on('latencyData', (body) => {
            console.log(body);
            setSocketArray((cur) => [...cur, body]);
        });

        return () => {
            socket.off('latencyData');
        };
    }, [testSocketArray]);

    return (
        <div className="grid grid-cols-1 gap-x-3 gap-y-4 lg:grid-cols-1 xl:grid-cols-2 2xl:grid-cols-3 xl:gap-x-4">
            {Array.from(Array(6)).map((_, i) => (
                <ChartCard key={i} id={i} />
            ))}
        </div>
    );
};

export default LatencyAnalytics;
