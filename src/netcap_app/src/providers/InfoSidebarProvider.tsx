import { useState } from 'react';
import { createGenericStateContext } from './createGenericStateContext';
import { InfoSidebarType } from '../types/PCAPTypes';

const [useInfoSidebarContext, InfoSidebarContextProvider] =
    createGenericStateContext<
        [InfoSidebarType, React.Dispatch<React.SetStateAction<InfoSidebarType>>]
    >();

const InfoSidebarProvider = ({ children }: { children: React.ReactNode }) => {
    const [infoSidebarState, setInfoSidebarState] = useState<InfoSidebarType>({
        isOpen: false,
        state: {
            id: -1,
            order_id: -1,
            source_ip: '',
            gateway_ip: '',
            trader_in_sec: BigInt(-1),
            trader_in_nano: BigInt(-1),
            trader_out_sec: BigInt(-1),
            trader_out_nano: BigInt(-1),
            gateway_in_sec: BigInt(-1),
            gateway_in_nano: BigInt(-1),
            gateway_out_sec: BigInt(-1),
            gateway_out_nano: BigInt(-1),
            ome_in_sec: BigInt(-1),
            ome_in_nano: BigInt(-1),
            ome_out_sec: BigInt(-1),
            ome_out_nano: BigInt(-1),
            ticker_out_sec: BigInt(-1),
            ticker_out_nano: BigInt(-1),
            latency: -1,
        },
    });
    return (
        <InfoSidebarContextProvider
            value={[infoSidebarState, setInfoSidebarState]}
        >
            {children}
        </InfoSidebarContextProvider>
    );
};

export { InfoSidebarProvider, useInfoSidebarContext };
