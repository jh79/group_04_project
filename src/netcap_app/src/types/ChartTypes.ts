export type ChartType = {
    chart_id: number;
    type: string;
    interval: number;
    group_by: string;
    refresh_rate: number;
    history_length: number;
    stop: boolean;
};
