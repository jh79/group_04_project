export type PCAPType = {
    id: number;
    order_id: number;
    source_ip: string;
    gateway_ip: string;
    trader_in_sec: bigint;
    trader_in_nano: bigint;
    trader_out_sec: bigint;
    trader_out_nano: bigint;
    gateway_in_sec: bigint;
    gateway_in_nano: bigint;
    gateway_out_sec: bigint;
    gateway_out_nano: bigint;
    ome_in_sec: bigint;
    ome_in_nano: bigint;
    ome_out_sec: bigint;
    ome_out_nano: bigint;
    ticker_out_sec: bigint;
    ticker_out_nano: bigint;
    latency: number;
};

export type InfoSidebarType = {
    isOpen: boolean;
    state: PCAPType;
};

export type PCAPPaginationType = {
    pageIndex: number;
    pageSize: number;
};
