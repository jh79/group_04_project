import sys
import requests
import json
import signal
from subprocess import Popen, PIPE, STDOUT

p = Popen(
    "./netcap_upload",
    shell=True,
    stdin=PIPE,
    stdout=PIPE,
    stderr=STDOUT,
    universal_newlines=True)
print("the process is running")


try:
    for line in iter(p.stdout.readline, ''):
        try:
            res = requests.post(
                "http://192.168.56.101:3000/rawPCAPService",
                json=json.loads(line))
            print(f"Uploaded packet with response: {res.status_code}")
        except BaseException:
            print("Error parsing or sending the JSON, did you remember to remove root to libpcap? Try running \"sudo setcap cap_net_raw,cap_net_admin=eip ./netcap_upload\" then try again")
            sys.exit(1)
except KeyboardInterrupt:
    p.send_signal(signal.SIGINT)
    p.wait()
    out, err = p.communicate()
    print(out)
    sys.exit(0)
